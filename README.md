# gs = graphql server

A Java graphql server, just POC

## Setup
Load the folder or `pom.xml` Into intelliJ
Build the project
Run the `main()` in `GsApplication.js`

Visit  http://localhost:8080/graphiql

Try the query in `bookDetails.graphql`
