package ca.hai5.gs;

public record Author (String id, String firstName, String lastName) {

    private static java.util.List<Author> authors = java.util.Arrays.asList(
            new Author("author-1", "Joshua", "Bloch"),
            new Author("author-2", "Douglas", "Adams"),
            new Author("author-3", "Billy", "Bryson")
    );

    public static Author getById(String id) {
        return authors.stream()
                .filter(author -> author.id().equals(id))
                .findFirst()
                .orElse(null);
    }
}
